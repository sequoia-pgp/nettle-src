# Convenience targets for updating the Nettle sources.

BASE_URL	?= https://ftp.gnu.org/gnu/nettle/
VERSION		?= $(shell grep '^version[[:space:]]*=[[:space:]]*' Cargo.toml | cut -d'"' -f2 | cut -d'-' -f1)

# Consider signatures before this date invalid.  Update every now and
# then.
NOT_BEFORE	= 2019-06-27

all: nettle

nettle: nettle-$(VERSION).tar.gz.sig nettle-$(VERSION).tar.gz
	sqv --not-before $(NOT_BEFORE) --keyring niels_moeller.pgp $?
	mkdir $@
	cd $@ && tar xf ../nettle-$(VERSION).tar.gz --strip 1
	find $@ -name '.*' -type f -execdir sh -c 'mv -v "{}" "HIDDEN$$(basename {})"' \;

nettle-$(VERSION).tar.gz.sig nettle-$(VERSION).tar.gz:
	wget "$(BASE_URL)$@"

.PHONY: clean
clean:
	rm -rf nettle nettle-*.tar.gz nettle-*.tar.gz.sig

# Cargo convenience.
#
# We need to pass --allow-dirty, because 'nettle' is not version
# controlled.

.PHONY: package
package:
	cargo package --allow-dirty

.PHONY: publish
publish:
	cargo publish --allow-dirty
