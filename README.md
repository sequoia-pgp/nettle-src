# nettle-src

This crate contains the source code of the Nettle cryptographic
library.  It is used by the `nettle-sys` crate if the `vendored`
feature is used.
