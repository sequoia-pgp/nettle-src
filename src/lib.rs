//! This is adapted from the openssl-src package.

use std::env;
use std::fs;
use std::path::{PathBuf, Path};
use std::process::Command;

use cc;

pub fn source_dir() -> PathBuf {
    Path::new(env!("CARGO_MANIFEST_DIR")).join("nettle")
}

pub fn version() -> &'static str {
    env!("CARGO_PKG_VERSION")
}

pub struct Build {
    out_dir: Option<PathBuf>,
    target: Option<String>,
    host: Option<String>,
}

pub struct Artifacts {
    include_dir: PathBuf,
    lib_dir: PathBuf,
    libs: Vec<String>,
}

impl Build {
    pub fn new() -> Build {
        Build {
            out_dir: env::var_os("OUT_DIR").map(|s| {
                PathBuf::from(s).join("nettle-build")
            }),
            target: env::var("TARGET").ok(),
            host: env::var("HOST").ok(),
        }
    }

    pub fn out_dir<P: AsRef<Path>>(&mut self, path: P) -> &mut Build {
        self.out_dir = Some(path.as_ref().to_path_buf());
        self
    }

    pub fn target(&mut self, target: &str) -> &mut Build {
        self.target = Some(target.to_string());
        self
    }

    pub fn host(&mut self, host: &str) -> &mut Build {
        self.host = Some(host.to_string());
        self
    }

    fn cmd_make(&self) -> Command {
        match &self.host.as_ref().expect("HOST dir not set")[..] {
            "x86_64-unknown-dragonfly" => Command::new("gmake"),
            "x86_64-unknown-freebsd" => Command::new("gmake"),
            _ => Command::new("make"),
        }
    }

    pub fn build(&mut self) -> Artifacts {
        fixup_sources(&source_dir());
        let target = &self.target.as_ref().expect("TARGET dir not set")[..];
        let host = &self.host.as_ref().expect("HOST dir not set")[..];
        let out_dir = self.out_dir.as_ref().expect("OUT_DIR not set");
        let build_dir = out_dir.join("build");
        let install_dir = out_dir.join("install");
        let lib_dir = install_dir.join("lib");
        let include_dir = install_dir.join("include");

        if build_dir.exists() {
            fs::remove_dir_all(&build_dir).unwrap();
        }
        if install_dir.exists() {
            fs::remove_dir_all(&install_dir).unwrap();
        }
        fs::create_dir_all(&build_dir).unwrap();

        let mut configure = Command::new(source_dir().join("configure"));
        if host.contains("pc-windows-gnu") {
            configure.arg(&format!("--prefix={}", sanitize_sh(&install_dir)));
        } else {
            configure.arg(&format!("--prefix={}", install_dir.display()));
        }

        configure
            // No shared objects, we just want static libraries
            .arg("--disable-shared")
            .arg("--disable-documentation")
            .arg("--libdir").arg(&lib_dir)
            .arg("--includedir").arg(&include_dir);

        // If we're not on MSVC we configure cross compilers and cross tools and
        // whatnot. Note that this doesn't happen on MSVC b/c things are pretty
        // different there and this isn't needed most of the time anyway.
        if !target.contains("msvc") {
            let mut cc = cc::Build::new();
            cc.target(target)
                .host(host)
                .warnings(false)
                .opt_level(2);
            let compiler = cc.get_compiler();
            configure.env("CC", compiler.path());
            let path = compiler.path().to_str().unwrap();

            // Infer ar/ranlib tools from cross compilers if the it looks like
            // we're doing something like `foo-gcc` route that to `foo-ranlib`
            // as well.
            if path.ends_with("-gcc") && !target.contains("unknown-linux-musl") {
                let path = &path[..path.len() - 4];
                configure.env("RANLIB", format!("{}-ranlib", path));
                configure.env("AR", format!("{}-ar", path));
            }
        }

        // And finally, run the configure script!
        configure.current_dir(&build_dir);
        self.run_command(configure, "configuring Nettle build");

        // On MSVC we use `nmake.exe` with a slightly different invocation, so
        // have that take a different path than the standard `make` below.
        if target.contains("msvc") {
            let mut build = cc::windows_registry::find(target, "nmake.exe")
                .expect("failed to find nmake");
            build.current_dir(&build_dir);
            self.run_command(build, "building Nettle");

            let mut install = cc::windows_registry::find(target, "nmake.exe")
                .expect("failed to find nmake");
            install.arg("install").current_dir(&build_dir);
            self.run_command(install, "installing Nettle");
        } else {
            let mut build = self.cmd_make();
            build.current_dir(&build_dir);
            if !cfg!(windows) {
                if let Some(s) = env::var_os("CARGO_MAKEFLAGS") {
                    build.env("MAKEFLAGS", s);
                }
            }
            self.run_command(build, "building Nettle");

            let mut install = self.cmd_make();
            install.arg("install").current_dir(&build_dir);
            self.run_command(install, "installing Nettle");
        }

        let libs = if target.contains("msvc") {
            vec!["libnettle".to_string(), "libhogweed".to_string()]
        } else {
            vec!["nettle".to_string(), "hogweed".to_string()]
        };

        fs::remove_dir_all(&build_dir).unwrap();

        Artifacts {
            lib_dir: lib_dir,
            include_dir: include_dir,
            libs: libs,
        }
    }

    fn run_command(&self, mut command: Command, desc: &str) {
        println!("running {:?}", command);
        let status = command.status().unwrap();
        if !status.success() {
            panic!("


Error {}:
    Command: {:?}
    Exit status: {}


    ",
                desc,
                command,
                status);
        }
    }
}

fn sanitize_sh(path: &Path) -> String {
    if !cfg!(windows) {
        return path.to_str().unwrap().to_string()
    }
    let path = path.to_str().unwrap().replace("\\", "/");
    return change_drive(&path).unwrap_or(path);

    fn change_drive(s: &str) -> Option<String> {
        let mut ch = s.chars();
        let drive = ch.next().unwrap_or('C');
        if ch.next() != Some(':') {
            return None
        }
        if ch.next() != Some('/') {
            return None
        }
        Some(format!("/{}/{}", drive, &s[drive.len_utf8() + 2..]))
    }
}

impl Artifacts {
    pub fn include_dir(&self) -> &Path {
        &self.include_dir
    }

    pub fn lib_dir(&self) -> &Path {
        &self.lib_dir
    }

    pub fn libs(&self) -> &[String] {
        &self.libs
    }

    pub fn print_cargo_metadata(&self) {
        println!("cargo:rustc-link-search=native={}", self.lib_dir.display());
        for lib in self.libs.iter() {
            println!("cargo:rustc-link-lib=static={}", lib);
        }
        println!("cargo:include={}", self.include_dir.display());
        println!("cargo:lib={}", self.lib_dir.display());
    }
}

/// Fixes problems with the source distribution.
///
/// Currently, `cargo package` excludes hidden files.  Our workaround
/// is to rename hidden files before we create the source
/// distribution, and reverse the process here.
///
/// https://github.com/rust-lang/cargo/issues/7183
fn fixup_sources(src: &Path) {
    for f in fs::read_dir(src).unwrap() {
        let f = f.unwrap();
        let path = f.path();
        let name = path.file_name().unwrap();

        if let Some(name) = name.to_str() {
            if name.starts_with("HIDDEN.") {
                let to = path.parent().unwrap().join(&name["HIDDEN".len()..]);
                fs::rename(&path, &to).unwrap();
                eprintln!("renamed {:?} -> {:?}", path, to);
            }
        }

        if f.file_type().unwrap().is_dir() {
            fixup_sources(&path);
        }
    }
}
